package com.userservice.UserService.repositories;

        import com.userservice.UserService.entities.UserEntity;
        import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<UserEntity,Long> {

}
