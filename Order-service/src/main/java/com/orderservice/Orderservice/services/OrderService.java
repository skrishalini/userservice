package com.orderservice.Orderservice.services;

import com.orderservice.Orderservice.dto.OrderDTO;
import com.orderservice.Orderservice.repositories.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class OrderService {

    @Autowired
    private OrderRepository repository;

    public List<OrderDTO> getAllOrders(){
        List<OrderDTO> orders = repository.findAll().stream().map(
                orderEntity -> new OrderDTO(
                        orderEntity.getId().toString(),
                        orderEntity.getOrder_id(),
                        orderEntity.getUser_id().toString()
                )
        ).collect(Collectors.toList());
        return orders;
    }

    public List<OrderDTO> getOrdersByUserId(long id){
        List<OrderDTO> orders = repository.findOrdersByUserId(id).stream().map(
                orderEntity -> new OrderDTO(
                        orderEntity.getId().toString(),
                        orderEntity.getOrder_id(),
                        orderEntity.getUser_id().toString()
                )
        ).collect(Collectors.toList());
        return orders;
    }

}
